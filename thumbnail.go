package thumbnail

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
	"strings"
	"time"

	"github.com/disintegration/imaging"
	"github.com/kpango/glg"
	"github.com/nfnt/resize"
)

//Thumbnail variable for keep thumbnail info
type Thumbnail struct {
	Name      string    `json:"Name"`
	Encoded   string    `json:"Encoded"`
	TimeStamp time.Time `json:"TimeStamp"`
	Decoded   image.Image
}

//CropImg to crop image with ratio
func (img *Thumbnail) CropImg(image image.Image, ratioW, ratioH int) image.Image {
	srcBounds := image.Bounds()
	//srcW := srcBounds.Dx()
	srcH := srcBounds.Dy()

	widthNewAR := (srcH / 3) * 4
	m := imaging.CropCenter(image, int(widthNewAR), srcH)

	return m
}

//ExportJPG image to jpg file with width and height
func (img *Thumbnail) ExportJPG(code, filename, path string, decodedImg image.Image, height, width uint, quality int) error {

	var cropList []string
	codes := strings.Split(os.Getenv("CHANNEL_AR43"), ",")

	for _, code := range codes {
		cropList = append(cropList, strings.Trim(code, " "))
	}

	thumbnail := decodedImg

	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, os.ModePerm)
		if err != nil {
			//log.Fatalln(err)
			glg.Failf("Can not mkdir:", err)
			return err
		}

		err = os.Chown(path, 1004, 1004)
		if err != nil {
			glg.Failf("Can not chown:", err)
			return err
		}

	}

	out, err := os.Create(path + filename)
	if err != nil {
		return err
	} else {
		defer out.Close()
		srcBounds := thumbnail.Bounds()
		srcW := srcBounds.Dx()
		srcH := srcBounds.Dy()

		if height == 0 && width == 0 {
			height = uint(srcH)
			width = uint(srcW)
		}

		glg.Infof("Size Original Image Code %s: %d * %d", code, srcW, srcH)

		var m image.Image

		glg.Infof("Resize Code %s with Lanczos3 ", code)
		m = resize.Resize(width, height, thumbnail, resize.Lanczos3)

		if quality == 0 {
			quality = 100
		}

		err = imaging.Save(m, path+filename, imaging.JPEGQuality(quality))
		if err != nil {
			log.Fatalf("failed to save image: %v", err)
		}
		// err = jpeg.Encode(out, m, nil)

		// if err != nil {
		// 	log.Fatalln(err)
		// }
	}
	return nil
}
